from tkinter import Tk
from fuzzy_app import FuzzyApp

if __name__ == "__main__":
    root = Tk()
    app = FuzzyApp(root)
    root.mainloop()
