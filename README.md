# Sistema Lógico Difuso Dinámico

## Objetivo del Programa

Este programa permite la creación y simulación de un sistema lógico difuso de manera dinámica. A través de una interfaz gráfica, los usuarios pueden definir variables, funciones de pertenencia, reglas y ejecutar el sistema difuso para obtener resultados. Además, permite visualizar las funciones de pertenencia y el resultado del sistema en gráficos, incluyendo una visualización 3D de la superficie de resultado con líneas de referencia para las variables de entrada.

## Creación de un Entorno Virtual con Python

Para crear un entorno virtual en Python, sigue los siguientes pasos:

1. Abre una terminal o línea de comandos.
2. Navega al directorio donde deseas crear el entorno virtual.
3. Ejecuta el siguiente comando para crear el entorno virtual:

```bash
python -m venv nombre_entorno
```

4. Activa el entorno virtual:
   - En Windows:

```bash
.\nombre_entorno\Scripts\activate
```

   - En macOS y Linux:

```bash
source nombre_entorno/bin/activate
```

## Instalación de las Librerías Usando un Archivo `requirements.txt`

1. Asegúrate de que el entorno virtual esté activado.
2. Coloca el archivo `requirements.txt` en el mismo directorio donde se encuentra el entorno virtual.
3. Ejecuta el siguiente comando para instalar las librerías necesarias:

```bash
pip install -r requirements.txt
```

## Ejecución del Programa

1. Asegúrate de que el entorno virtual esté activado.
2. Navega al directorio donde se encuentra el archivo `main.py`.
3. Ejecuta el programa con el siguiente comando:

```bash
python main.py
```

## Uso del Programa

### Definición de Variables

1. En la sección "Definición de Variables", introduce el nombre de la variable, selecciona el tipo (Antecedente o Consecuente), y define el rango de inicio y fin.
2. Haz clic en "Agregar Variable" para añadir la variable al sistema.
3. Las variables definidas aparecerán en una lista en la misma sección.

### Definición de Funciones de Pertenencia

1. En la sección "Definición de Funciones de Pertenencia", selecciona la variable a la que se asociará la función de pertenencia.
2. Introduce el nombre de la función y selecciona el tipo (trimf, trapmf, gaussmf).
3. Define los parámetros correspondientes y haz clic en "Agregar Función de Pertenencia".
4. Las funciones de pertenencia definidas aparecerán en una lista en la misma sección.

### Definición de Reglas

1. En la sección "Definición de Reglas", selecciona los antecedentes y el consecuente de la regla.
2. Selecciona el operador (AND, OR) y haz clic en "Agregar Regla".
3. Las reglas definidas aparecerán en una lista en la misma sección.

### Ejecución del Sistema

1. Haz clic en "Ejecutar Sistema" para configurar el sistema difuso con las reglas definidas.
2. En la sección "Parámetros y Cálculo", introduce los valores de las variables de entrada.
3. Haz clic en "Calcular" para obtener los resultados.
4. Los resultados se mostrarán en la misma sección y se visualizarán las gráficas correspondientes.

## Archivos .py

### `main.py`

Este es el archivo principal que inicia la aplicación. Contiene el código necesario para configurar y lanzar la interfaz gráfica.

```python
from tkinter import Tk
from fuzzy_app import FuzzyApp

if __name__ == "__main__":
    root = Tk()
    app = FuzzyApp(root)
    root.mainloop()
```

### `fuzzy_app.py`

Este archivo contiene la clase `FuzzyApp`, que define la interfaz gráfica y maneja todas las interacciones del usuario con el programa. Aquí se definen los métodos para agregar variables, funciones de pertenencia, reglas y ejecutar el sistema difuso.

Principales funciones y clases:
- `__init__`: Inicializa la interfaz y configura los componentes principales.
- `create_widgets`: Crea y organiza todos los widgets de la interfaz.
- `update_params`: Actualiza los parámetros de las funciones de pertenencia según el tipo seleccionado.
- `validate_number`: Valida que los valores de los rangos y parámetros sean números.
- `add_variable`, `add_membership_function`, `add_rule`: Métodos para agregar variables, funciones de pertenencia y reglas respectivamente.
- `delete_variable`, `delete_membership_function`, `delete_rule`: Métodos para eliminar variables, funciones de pertenencia y reglas respectivamente.
- `clear_all`: Limpia todas las definiciones y restablece la interfaz.
- `clear_all_rules`: Limpia todas las reglas definidas.
- `set_example`: Configura un ejemplo predefinido para mostrar el uso del sistema.
- `run_system`: Configura y ejecuta el sistema difuso, limpia los campos de entrada y muestra los resultados.
- `compute_output`: Calcula y muestra los resultados del sistema difuso, además de generar las gráficas correspondientes.

### `fuzzy_util.py`

Este archivo contiene funciones auxiliares para agregar y eliminar variables y funciones de pertenencia, y para actualizar los menús de reglas.

Principales funciones:
- `add_variable`: Agrega una variable difusa (antecedente o consecuente) al sistema.
- `add_membership_function`: Agrega una función de pertenencia a una variable difusa.
- `delete_variable`: Elimina una variable difusa del sistema.
- `delete_membership_function`: Elimina una función de pertenencia de una variable difusa.
- `delete_rule`: Elimina una regla difusa del sistema.
- `clear_all`: Limpia todas las definiciones y restablece la interfaz.
- `update_rule_menus`: Actualiza los menús de reglas con las variables y funciones de pertenencia definidas.

### `fuzzy_plot.py`

Este archivo contiene funciones para generar gráficos de las funciones de pertenencia y la superficie resultante del sistema difuso.

Principales funciones:
- `plot_all_membership_functions`: Genera gráficos de todas las funciones de pertenencia definidas.
- `plot_resulting_surface`: Genera un gráfico 3D de la superficie resultante del sistema difuso, incluyendo líneas de referencia para las variables de entrada.
- `plot_resulting_membership_function`: Genera un gráfico de la función de pertenencia resultante para la variable consecuente.

### `fuzzy_example.py`

Este archivo contiene una función para configurar un ejemplo predefinido de un sistema difuso.

Principales funciones:
- `set_example_data`: Configura un sistema difuso de ejemplo con variables, funciones de pertenencia y reglas predefinidas.

## Requisitos Adicionales

- `numpy`
- `scikit-fuzzy`
- `matplotlib`
- `tkinter` (incluido con Python estándar)

Asegúrate de tener estas librerías instaladas utilizando el archivo `requirements.txt`.