import tkinter as tk
from tkinter import ttk, messagebox
import matplotlib.pyplot as plt
import numpy as np
from skfuzzy import control as ctrl
from fuzzy_example import set_example_data
from fuzzy_plot import plot_all_membership_functions, plot_resulting_surface, plot_resulting_membership_function
from fuzzy_util import add_variable, add_membership_function, clear_all, delete_variable, delete_membership_function, delete_rule, get_variable_terms, update_rule_menus

class FuzzyApp:
    def __init__(self, root):
        self.root = root
        self.root.title("Sistema Lógico Difuso Dinámico")
        self.root.configure(bg="#f0f0f0")
        self.root.geometry(f"{self.root.winfo_screenwidth()}x{self.root.winfo_screenheight()}")

        self.variables = {}
        self.rules = []
        self.control_system = None

        # Crear un canvas con scroll
        self.main_frame = tk.Frame(root, bg="#f0f0f0")
        self.main_frame.pack(fill=tk.BOTH, expand=1)

        self.canvas = tk.Canvas(self.main_frame, bg="#f0f0f0")
        self.canvas.pack(side=tk.LEFT, fill=tk.BOTH, expand=1)

        self.scrollbar = ttk.Scrollbar(self.main_frame, orient=tk.VERTICAL, command=self.canvas.yview)
        self.scrollbar.pack(side=tk.RIGHT, fill=tk.Y)

        self.canvas.configure(yscrollcommand=self.scrollbar.set)
        self.canvas.bind('<Configure>', lambda e: self.canvas.configure(scrollregion=self.canvas.bbox('all')))

        self.second_frame = tk.Frame(self.canvas, bg="#f0f0f0")
        self.canvas.create_window((0, 0), window=self.second_frame, anchor="nw")

        self.create_widgets()

    def create_widgets(self):
        # Variables Frame
        self.var_frame = tk.LabelFrame(self.second_frame, text="Definición de Variables", bg="#d9eaf7", font=("Helvetica", 12, "bold"))
        self.var_frame.grid(row=0, column=0, padx=10, pady=10, sticky="n")

        self.var_name_var = tk.StringVar()
        self.var_type_var = tk.StringVar(value="Antecedente")
        self.var_range_start_var = tk.StringVar()
        self.var_range_end_var = tk.StringVar()

        tk.Label(self.var_frame, text="Nombre:", bg="#d9eaf7").grid(row=0, column=0, sticky="e")
        tk.Entry(self.var_frame, textvariable=self.var_name_var).grid(row=0, column=1)
        tk.Label(self.var_frame, text="Tipo:", bg="#d9eaf7").grid(row=1, column=0, sticky="e")
        tk.OptionMenu(self.var_frame, self.var_type_var, "Antecedente", "Consecuente").grid(row=1, column=1)
        tk.Label(self.var_frame, text="Rango Inicio:", bg="#d9eaf7").grid(row=2, column=0, sticky="e")
        tk.Entry(self.var_frame, textvariable=self.var_range_start_var).grid(row=2, column=1)
        tk.Label(self.var_frame, text="Rango Fin:", bg="#d9eaf7").grid(row=3, column=0, sticky="e")
        tk.Entry(self.var_frame, textvariable=self.var_range_end_var).grid(row=3, column=1)
        tk.Button(self.var_frame, text="Agregar Variable", command=self.add_variable, bg="#4CAF50", fg="white").grid(row=4, column=0, columnspan=2, pady=5)
        tk.Button(self.var_frame, text="Eliminar Variable", command=self.delete_variable, bg="#f44336", fg="white").grid(row=5, column=0, columnspan=2, pady=5)

        self.var_listbox = tk.Listbox(self.var_frame, width=50)
        self.var_listbox.grid(row=6, column=0, columnspan=2, pady=5)
        self.var_listbox.bind('<Double-Button-1>', self.edit_variable)

        # Membership Functions Frame
        self.mf_frame = tk.LabelFrame(self.second_frame, text="Definición de Funciones de Pertenencia", bg="#d9eaf7", font=("Helvetica", 12, "bold"))
        self.mf_frame.grid(row=0, column=1, padx=10, pady=10, sticky="n")

        self.mf_var_name_var = tk.StringVar()
        self.mf_name_var = tk.StringVar()
        self.mf_type_var = tk.StringVar(value="trimf")
        self.mf_param1_var = tk.StringVar()
        self.mf_param2_var = tk.StringVar()
        self.mf_param3_var = tk.StringVar()
        self.mf_param4_var = tk.StringVar()

        tk.Label(self.mf_frame, text="Variable:", bg="#d9eaf7").grid(row=0, column=0, sticky="e")
        self.mf_var_menu = tk.OptionMenu(self.mf_frame, self.mf_var_name_var, '')
        self.mf_var_menu.grid(row=0, column=1)
        tk.Label(self.mf_frame, text="Nombre:", bg="#d9eaf7").grid(row=1, column=0, sticky="e")
        tk.Entry(self.mf_frame, textvariable=self.mf_name_var).grid(row=1, column=1)
        tk.Label(self.mf_frame, text="Tipo:", bg="#d9eaf7").grid(row=2, column=0, sticky="e")
        tk.OptionMenu(self.mf_frame, self.mf_type_var, "trimf", "trapmf", "gaussmf", command=self.update_params).grid(row=2, column=1)

        self.param_frame = tk.Frame(self.mf_frame, bg="#d9eaf7")
        self.param_frame.grid(row=3, column=0, columnspan=2, pady=5)

        self.update_params(self.mf_type_var.get())

        tk.Button(self.mf_frame, text="Agregar Función de Pertenencia", command=self.add_membership_function, bg="#4CAF50", fg="white").grid(row=4, column=0, columnspan=2, pady=5)
        tk.Button(self.mf_frame, text="Eliminar Función de Pertenencia", command=self.delete_membership_function, bg="#f44336", fg="white").grid(row=5, column=0, columnspan=2, pady=5)

        self.mf_listbox = tk.Listbox(self.mf_frame, width=50)
        self.mf_listbox.grid(row=6, column=0, columnspan=2, pady=5)
        self.mf_listbox.bind('<Double-Button-1>', self.edit_membership_function)

        # Rules Frame
        self.rule_frame = tk.LabelFrame(self.second_frame, text="Definición de Reglas", bg="#d9eaf7", font=("Helvetica", 12, "bold"))
        self.rule_frame.grid(row=0, column=2, padx=10, pady=10, sticky="n")

        self.rule_antecedent1_var = tk.StringVar()
        self.rule_antecedent2_var = tk.StringVar()
        self.rule_operator_var = tk.StringVar(value="AND")
        self.rule_consequent_var = tk.StringVar()

        tk.Label(self.rule_frame, text="Antecedente 1:", bg="#d9eaf7").grid(row=0, column=0, sticky="e")
        self.rule_antecedent1_menu = tk.OptionMenu(self.rule_frame, self.rule_antecedent1_var, '')
        self.rule_antecedent1_menu.grid(row=0, column=1)

        tk.Label(self.rule_frame, text="Operador:", bg="#d9eaf7").grid(row=1, column=0, sticky="e")
        tk.OptionMenu(self.rule_frame, self.rule_operator_var, "AND", "OR").grid(row=1, column=1)

        tk.Label(self.rule_frame, text="Antecedente 2:", bg="#d9eaf7").grid(row=2, column=0, sticky="e")
        self.rule_antecedent2_menu = tk.OptionMenu(self.rule_frame, self.rule_antecedent2_var, '')
        self.rule_antecedent2_menu.grid(row=2, column=1)

        tk.Label(self.rule_frame, text="Consecuente:", bg="#d9eaf7").grid(row=3, column=0, sticky="e")
        self.rule_consequent_menu = tk.OptionMenu(self.rule_frame, self.rule_consequent_var, '')
        self.rule_consequent_menu.grid(row=3, column=1)

        tk.Button(self.rule_frame, text="Agregar Regla", command=self.add_rule, bg="#4CAF50", fg="white").grid(row=4, column=0, columnspan=2, pady=5)
        tk.Button(self.rule_frame, text="Eliminar Regla", command=self.delete_rule, bg="#f44336", fg="white").grid(row=5, column=0, columnspan=2, pady=5)

        self.rule_listbox = tk.Listbox(self.rule_frame, width=50)
        self.rule_listbox.grid(row=6, column=0, columnspan=2, pady=5)

        # Buttons Frame
        self.button_frame = tk.Frame(self.second_frame, bg="#f0f0f0")
        self.button_frame.grid(row=1, column=0, padx=10, pady=10, sticky="n")

        tk.Button(self.button_frame, text="Ejecutar Sistema", command=self.run_system, bg="#2196F3", fg="white", font=("Helvetica", 10, "bold")).grid(row=0, column=0, pady=5)
        tk.Button(self.button_frame, text="Borrar Todo", command=self.clear_all, bg="#f44336", fg="white", font=("Helvetica", 10, "bold")).grid(row=1, column=0, pady=5)
        tk.Button(self.button_frame, text="Ejemplo", command=self.set_example, bg="#FF9800", fg="white", font=("Helvetica", 10, "bold")).grid(row=2, column=0, pady=5)
        tk.Button(self.button_frame, text="Graficar Funciones", command=self.plot_all_membership_functions, bg="#9C27B0", fg="white", font=("Helvetica", 10, "bold")).grid(row=3, column=0, pady=5)

        # Calculation Frame
        self.calc_frame = tk.LabelFrame(self.second_frame, text="Parámetros y Cálculo", bg="#d9eaf7", font=("Helvetica", 12, "bold"))

    def update_params(self, mf_type):
        for widget in self.param_frame.winfo_children():
            widget.destroy()

        if mf_type == "trimf":
            self.create_param_entry(self.param_frame, "Param1 (a):", self.mf_param1_var, "Parámetro 1 de la función triangular (a).")
            self.create_param_entry(self.param_frame, "Param2 (b):", self.mf_param2_var, "Parámetro 2 de la función triangular (b).")
            self.create_param_entry(self.param_frame, "Param3 (c):", self.mf_param3_var, "Parámetro 3 de la función triangular (c).")
        elif mf_type == "trapmf":
            self.create_param_entry(self.param_frame, "Param1 (a):", self.mf_param1_var, "Parámetro 1 de la función trapezoidal (a).")
            self.create_param_entry(self.param_frame, "Param2 (b):", self.mf_param2_var, "Parámetro 2 de la función trapezoidal (b).")
            self.create_param_entry(self.param_frame, "Param3 (c):", self.mf_param3_var, "Parámetro 3 de la función trapezoidal (c).")
            self.create_param_entry(self.param_frame, "Param4 (d):", self.mf_param4_var, "Parámetro 4 de la función trapezoidal (d).")
        elif mf_type == "gaussmf":
            self.create_param_entry(self.param_frame, "Param1 (mean):", self.mf_param1_var, "Media de la función gaussiana.")
            self.create_param_entry(self.param_frame, "Param2 (sigma):", self.mf_param2_var, "Desviación estándar de la función gaussiana.")

    def create_param_entry(self, frame, label_text, variable, tooltip_text):
        label = tk.Label(frame, text=label_text, bg="#d9eaf7")
        label.grid(row=len(frame.grid_slaves()), column=0, sticky="e")
        entry = tk.Entry(frame, textvariable=variable)
        entry.grid(row=len(frame.grid_slaves()) - 1, column=1)
        self.create_tooltip(entry, tooltip_text)

    def create_tooltip(self, widget, text):
        tooltip = tk.Label(widget, text=text, bg="#ffffe0", relief="solid", bd=1, padx=2, pady=2)
        tooltip.place_forget()
        
        def show_tooltip(event):
            tooltip.place(x=widget.winfo_x(), y=widget.winfo_y() + widget.winfo_height())

        def hide_tooltip(event):
            tooltip.place_forget()

        widget.bind("<Enter>", show_tooltip)
        widget.bind("<Leave>", hide_tooltip)

    def validate_number(self, value):
        try:
            float(value)
            return True
        except ValueError:
            return False

    def add_variable(self):
        if not self.validate_number(self.var_range_start_var.get()) or not self.validate_number(self.var_range_end_var.get()):
            messagebox.showerror("Error", "Los rangos de las variables deben ser números.")
            return
        if self.var_type_var.get() == "Antecedente" and len([v for v in self.variables.values() if isinstance(v, ctrl.Antecedent)]) >= 2:
            messagebox.showerror("Error", "Solo se permiten dos variables antecedentes.")
            return
        if self.var_type_var.get() == "Consecuente" and any(isinstance(v, ctrl.Consequent) for v in self.variables.values()):
            messagebox.showerror("Error", "Solo se permite una variable consecuente.")
            return
        add_variable(self)
        self.update_rule_menus()

    def add_membership_function(self):
        params = [self.mf_param1_var.get(), self.mf_param2_var.get(), self.mf_param3_var.get(), self.mf_param4_var.get()]
        if not all(self.validate_number(param) for param in params if param):
            messagebox.showerror("Error", "Los parámetros de las funciones de pertenencia deben ser números.")
            return
        add_membership_function(self)
        self.update_rule_menus()

    def add_rule(self):
        antecedent_var1, antecedent_mf1 = self.rule_antecedent1_var.get().split(": ")
        antecedent_var2, antecedent_mf2 = self.rule_antecedent2_var.get().split(": ")
        consequent_var, consequent_mf = self.rule_consequent_var.get().split(": ")
        
        if self.rule_operator_var.get() == "AND":
            rule = ctrl.Rule(self.variables[antecedent_var1][antecedent_mf1] & self.variables[antecedent_var2][antecedent_mf2], self.variables[consequent_var][consequent_mf])
        else:
            rule = ctrl.Rule(self.variables[antecedent_var1][antecedent_mf1] | self.variables[antecedent_var2][antecedent_mf2], self.variables[consequent_var][consequent_mf])

        self.rules.append(rule)
        self.rule_listbox.insert(tk.END, f"SI {self.rule_antecedent1_var.get()} {self.rule_operator_var.get()} {self.rule_antecedent2_var.get()} ENTONCES {self.rule_consequent_var.get()}")

    def get_variable_terms(self):
        terms = []
        for key in self.variables.keys():
            for term in self.variables[key].terms:
                terms.append(f"{key}: {term}")
        return terms

    def update_rule_menus(self):
        terms = self.get_variable_terms()
        self.rule_antecedent1_menu['menu'].delete(0, 'end')
        self.rule_antecedent2_menu['menu'].delete(0, 'end')
        self.rule_consequent_menu['menu'].delete(0, 'end')

        for term in terms:
            self.rule_antecedent1_menu['menu'].add_command(label=term, command=tk._setit(self.rule_antecedent1_var, term))
            self.rule_antecedent2_menu['menu'].add_command(label=term, command=tk._setit(self.rule_antecedent2_var, term))
            self.rule_consequent_menu['menu'].add_command(label=term, command=tk._setit(self.rule_consequent_var, term))

    def delete_variable(self):
        delete_variable(self)
        self.update_rule_menus()

    def delete_membership_function(self):
        delete_membership_function(self)
        self.update_rule_menus()

    def delete_rule(self):
        selected_rule_index = self.rule_listbox.curselection()
        if not selected_rule_index:
            return
        selected_rule_index = selected_rule_index[0]
        del self.rules[selected_rule_index]
        self.rule_listbox.delete(selected_rule_index)

    def clear_all(self):
        clear_all(self)
        self.var_name_var.set('')
        self.var_type_var.set('Antecedente')
        self.var_range_start_var.set('')
        self.var_range_end_var.set('')
        self.mf_var_name_var.set('')
        self.mf_name_var.set('')
        self.mf_type_var.set('trimf')
        self.mf_param1_var.set('')
        self.mf_param2_var.set('')
        self.mf_param3_var.set('')
        self.mf_param4_var.set('')
        self.rule_antecedent1_var.set('')
        self.rule_antecedent2_var.set('')
        self.rule_operator_var.set('AND')
        self.rule_consequent_var.set('')
        self.rules = []
        self.rule_listbox.delete(0, tk.END)
        for widget in self.second_frame.grid_slaves():
            if int(widget.grid_info()["row"]) > 4 and int(widget.grid_info()["column"]) >= 2:
                widget.grid_forget()
        self.calc_frame.grid_forget()  # Ocultar el panel de Parámetros y Cálculo

    def clear_all_rules(self):
        self.rules = []
        self.rule_listbox.delete(0, tk.END)

    def set_example(self):
        set_example_data(self)
        self.update_rule_menus()

    def plot_all_membership_functions(self):
        plot_all_membership_functions(self)

    def run_system(self):
        if not self.rules:
            messagebox.showerror("Error", "No se han definido reglas")
            return

        self.control_system = ctrl.ControlSystem(self.rules)
        self.simulation = ctrl.ControlSystemSimulation(self.control_system)

        input_vars = {var_name: tk.DoubleVar() for var_name in self.variables if isinstance(self.variables[var_name], ctrl.Antecedent)}
        input_widgets = {}

        # Limpiar campos de parámetros y cálculo
        for widget in self.calc_frame.winfo_children():
            widget.grid_forget()

        for i, (var_name, var) in enumerate(input_vars.items()):
            tk.Label(self.calc_frame, text=f"{var_name}:", bg="#f0f0f0").grid(row=4+i, column=0, sticky="e", padx=5, pady=2)
            entry = tk.Entry(self.calc_frame, textvariable=var)
            entry.grid(row=4+i, column=1, pady=2)
            input_widgets[var_name] = entry

        def compute_output():
            try:
                for var_name, var in input_vars.items():
                    self.simulation.input[var_name] = var.get()
                self.simulation.compute()
                output_text = "\n".join([f"{var_name}: {round(self.simulation.output[var_name], 3)}" for var_name in self.variables if isinstance(self.variables[var_name], ctrl.Consequent)])
                
                # Limpiar valores de resultado anteriores
                for widget in self.calc_frame.grid_slaves():
                    if int(widget.grid_info()["row"]) > 4 + len(input_vars):
                        widget.grid_forget()

                result_label = tk.Label(self.calc_frame, text="Resultados:", bg="#f0f0f0")
                result_label.grid(row=4+len(input_vars), column=0, sticky="e")
                result_output = tk.Label(self.calc_frame, text=output_text, bg="#f0f0f0", wraplength=250)
                result_output.grid(row=4+len(input_vars), column=1, sticky="w")

                # Programar la ejecución de las funciones de graficado en el hilo principal
                self.root.after(0, plot_resulting_surface, self)
                self.root.after(0, plot_resulting_membership_function, self)

            except Exception as e:
                messagebox.showerror("Error", f"Error al ejecutar el sistema: {e}")

        # Mover el botón "Calcular" debajo de los resultados
        calcular_button = tk.Button(self.calc_frame, text="Calcular", command=compute_output, bg="#2196F3", fg="white")
        calcular_button.grid(row=5+len(input_vars)+1, column=0, columnspan=2, pady=5)

        self.calc_frame.grid(row=1, column=1, padx=10, pady=10, sticky="n")  # Mostrar el panel de Parámetros y Cálculo

    def edit_variable(self, event):
        selected_index = self.var_listbox.curselection()
        if not selected_index:
            return
        selected_index = selected_index[0]
        selected_var = self.var_listbox.get(selected_index).split(": ")[1].split(" [")[0]
        self.var_name_var.set(selected_var)
        self.var_type_var.set("Antecedente" if "Antecedente" in self.var_listbox.get(selected_index) else "Consecuente")
        self.var_range_start_var.set(self.variables[selected_var].universe[0])
        self.var_range_end_var.set(self.variables[selected_var].universe[-1])

    def edit_membership_function(self, event):
        selected_index = self.mf_listbox.curselection()
        if not selected_index:
            return
        selected_index = selected_index[0]
        selected_mf = self.mf_listbox.get(selected_index).split(": ")[1].split(" (")[0]
        var_name = self.mf_listbox.get(selected_index).split(": ")[0]
        self.mf_var_name_var.set(var_name)
        self.mf_name_var.set(selected_mf)
        mf_params = self.variables[var_name][selected_mf].mf.params
        self.mf_param1_var.set(mf_params[0])
        if len(mf_params) > 1:
            self.mf_param2_var.set(mf_params[1])
        if len(mf_params) > 2:
            self.mf_param3_var.set(mf_params[2])
        if len(mf_params) > 3:
            self.mf_param4_var.set(mf_params[3])
        self.update_params(self.mf_type_var.get())
