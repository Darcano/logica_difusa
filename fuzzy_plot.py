import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
from tkinter import messagebox
from skfuzzy import control as ctrl
import skfuzzy as fuzz

def plot_all_membership_functions(app):
    plt.ioff()  # Desactivar modo interactivo
    fig, axes = plt.subplots(nrows=len(app.variables), figsize=(8, 3*len(app.variables)))
    
    if len(app.variables) == 1:
        axes = [axes]
    
    for ax, (var_name, var) in zip(axes, app.variables.items()):
        for term in var.terms:
            ax.plot(var.universe, var[term].mf, label=term)
        ax.set_title(var_name)
        ax.legend()

    plt.tight_layout()
    plt.show()
    plt.ion()  # Reactivar modo interactivo

def plot_resulting_surface(app):
    plt.ioff()  # Desactivar modo interactivo
    input_vars = [var for var in app.variables if isinstance(app.variables[var], ctrl.Antecedent)]
    output_vars = [var for var in app.variables if isinstance(app.variables[var], ctrl.Consequent)]
    
    valor1_input_grafica=float(str(app.simulation.input).split('\n')[0].split(':')[1])
    valor2_input_grafica=float(str(app.simulation.input).split('\n')[1].split(':')[1])
    valor_output_grafica = float(app.simulation.output[output_vars[0]])
    
    if len(input_vars) != 2 or len(output_vars) != 1:
        messagebox.showinfo("Info", "La visualización 3D solo es posible con 2 variables de entrada y 1 variable de salida.")
        return
    
    x = np.linspace(app.variables[input_vars[0]].universe.min(), app.variables[input_vars[0]].universe.max(), 30)
    y = np.linspace(app.variables[input_vars[1]].universe.min(), app.variables[input_vars[1]].universe.max(), 30)
    X, Y = np.meshgrid(x, y)
    Z = np.zeros_like(X)

    for i in range(30):
        for j in range(30):
            app.simulation.input[input_vars[0]] = X[i, j]
            app.simulation.input[input_vars[1]] = Y[i, j]
            app.simulation.compute()
            Z[i, j] = app.simulation.output[output_vars[0]]

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.plot_surface(X, Y, Z, cmap='viridis', alpha=0.7)

    # Asegúrate de que los valores de entrada se obtienen correctamente
    input_value1 = valor1_input_grafica
    input_value2 = valor2_input_grafica
    output_value = valor_output_grafica

    # Línea en X (variable 1)
    ax.plot([input_value1, input_value1], [y.min(), y.max()], [output_value, output_value], color='r', linestyle='--', linewidth=1)

    # Línea en Y (variable 2)
    ax.plot([x.min(), x.max()], [input_value2, input_value2], [output_value, output_value], color='r', linestyle='--', linewidth=1)

    # Punto del resultado
    ax.scatter(input_value1, input_value2, output_value, color='r', s=100)

    ax.set_xlabel(input_vars[0])
    ax.set_ylabel(input_vars[1])
    ax.set_zlabel(output_vars[0])
    plt.show()
    plt.ion()  # Reactivar modo interactivo
    
def plot_resulting_membership_function(app):
    plt.ioff()  # Desactivar modo interactivo
    consequent_vars = [var_name for var_name in app.variables if isinstance(app.variables[var_name], ctrl.Consequent)]
    if not consequent_vars:
        return

    var_name = consequent_vars[0]
    consequent_var = app.variables[var_name]
    universe = consequent_var.universe

    fig, ax = plt.subplots()
    for term_name, term in consequent_var.terms.items():
        ax.plot(universe, term.mf, label=term_name)

    # Evaluamos el sistema con los valores de entrada
    app.simulation.compute()
    result = app.simulation.output[var_name]

    # Obtenemos la salida difusa
    result_mf = np.zeros_like(universe)
    for term_name, term in consequent_var.terms.items():
        result_mf = np.maximum(result_mf, term.mf)

    ax.plot(universe, result_mf, 'r', linewidth=2, label=f'Resultado {var_name}')
    ax.set_title(f'Resultado Difuso de {var_name}')
    ax.legend()
    plt.show()
    plt.ion()  # Reactivar modo interactivo
