from fuzzy_util import add_variable, add_membership_function

def set_example_data(app):
    app.clear_all()
    
    # Agregar variables
    app.var_name_var.set("oferta")
    app.var_type_var.set("Antecedente")
    app.var_range_start_var.set(0)
    app.var_range_end_var.set(100)
    app.add_variable()

    app.var_name_var.set("demanda")
    app.var_type_var.set("Antecedente")
    app.var_range_start_var.set(0)
    app.var_range_end_var.set(100)
    app.add_variable()

    app.var_name_var.set("precio")
    app.var_type_var.set("Consecuente")
    app.var_range_start_var.set(0)
    app.var_range_end_var.set(100)
    app.add_variable()

    # Agregar funciones de pertenencia para 'oferta'
    app.mf_var_name_var.set("oferta")
    app.mf_name_var.set("baja")
    app.mf_type_var.set("trapmf")
    app.mf_param1_var.set(0)
    app.mf_param2_var.set(0)
    app.mf_param3_var.set(20)
    app.mf_param4_var.set(40)
    app.add_membership_function()

    app.mf_var_name_var.set("oferta")
    app.mf_name_var.set("media")
    app.mf_type_var.set("gaussmf")
    app.mf_param1_var.set(50)
    app.mf_param2_var.set(15)
    app.add_membership_function()

    app.mf_var_name_var.set("oferta")
    app.mf_name_var.set("alta")
    app.mf_type_var.set("trimf")
    app.mf_param1_var.set(60)
    app.mf_param2_var.set(80)
    app.mf_param3_var.set(100)
    app.add_membership_function()

    # Agregar funciones de pertenencia para 'demanda'
    app.mf_var_name_var.set("demanda")
    app.mf_name_var.set("baja")
    app.mf_type_var.set("trimf")
    app.mf_param1_var.set(0)
    app.mf_param2_var.set(20)
    app.mf_param3_var.set(40)
    app.add_membership_function()

    app.mf_var_name_var.set("demanda")
    app.mf_name_var.set("media")
    app.mf_type_var.set("trapmf")
    app.mf_param1_var.set(30)
    app.mf_param2_var.set(40)
    app.mf_param3_var.set(60)
    app.mf_param4_var.set(70)
    app.add_membership_function()

    app.mf_var_name_var.set("demanda")
    app.mf_name_var.set("alta")
    app.mf_type_var.set("gaussmf")
    app.mf_param1_var.set(80)
    app.mf_param2_var.set(10)
    app.add_membership_function()

    # Agregar funciones de pertenencia para 'precio'
    app.mf_var_name_var.set("precio")
    app.mf_name_var.set("bajo")
    app.mf_type_var.set("gaussmf")
    app.mf_param1_var.set(10)
    app.mf_param2_var.set(5)
    app.add_membership_function()

    app.mf_var_name_var.set("precio")
    app.mf_name_var.set("medio")
    app.mf_type_var.set("trimf")
    app.mf_param1_var.set(30)
    app.mf_param2_var.set(50)
    app.mf_param3_var.set(70)
    app.add_membership_function()

    app.mf_var_name_var.set("precio")
    app.mf_name_var.set("alto")
    app.mf_type_var.set("trapmf")
    app.mf_param1_var.set(60)
    app.mf_param2_var.set(80)
    app.mf_param3_var.set(100)
    app.mf_param4_var.set(100)
    app.add_membership_function()

    # Agregar reglas
    app.clear_all_rules()

    app.rule_antecedent1_var.set("oferta: baja")
    app.rule_operator_var.set("AND")
    app.rule_antecedent2_var.set("demanda: alta")
    app.rule_consequent_var.set("precio: bajo")
    app.add_rule()

    app.rule_antecedent1_var.set("oferta: media")
    app.rule_operator_var.set("AND")
    app.rule_antecedent2_var.set("demanda: media")
    app.rule_consequent_var.set("precio: medio")
    app.add_rule()

    app.rule_antecedent1_var.set("oferta: alta")
    app.rule_operator_var.set("AND")
    app.rule_antecedent2_var.set("demanda: baja")
    app.rule_consequent_var.set("precio: bajo")
    app.add_rule()

    app.rule_antecedent1_var.set("oferta: baja")
    app.rule_operator_var.set("AND")
    app.rule_antecedent2_var.set("demanda: media")
    app.rule_consequent_var.set("precio: medio")
    app.add_rule()

    app.rule_antecedent1_var.set("oferta: media")
    app.rule_operator_var.set("AND")
    app.rule_antecedent2_var.set("demanda: alta")
    app.rule_consequent_var.set("precio: medio")
    app.add_rule()

    app.rule_antecedent1_var.set("oferta: baja")
    app.rule_operator_var.set("AND")
    app.rule_antecedent2_var.set("demanda: baja")
    app.rule_consequent_var.set("precio: bajo")
    app.add_rule()

    app.rule_antecedent1_var.set("oferta: alta")
    app.rule_operator_var.set("AND")
    app.rule_antecedent2_var.set("demanda: alta")
    app.rule_consequent_var.set("precio: alto")
    app.add_rule()

    app.rule_antecedent1_var.set("oferta: alta")
    app.rule_operator_var.set("AND")
    app.rule_antecedent2_var.set("demanda: media")
    app.rule_consequent_var.set("precio: medio")
    app.add_rule()

    app.update_rule_menus()
