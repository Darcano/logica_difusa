import tkinter as tk
from tkinter import messagebox
import numpy as np
import skfuzzy as fuzz
from skfuzzy import control as ctrl

def add_variable(app):
    var_name = app.var_name_var.get()
    var_type = app.var_type_var.get()
    var_range = np.arange(float(app.var_range_start_var.get()), float(app.var_range_end_var.get()), 1)

    if var_type == "Antecedente":
        app.variables[var_name] = ctrl.Antecedent(var_range, var_name)
    else:
        app.variables[var_name] = ctrl.Consequent(var_range, var_name)

    app.var_listbox.insert(tk.END, f"{var_type}: {var_name} [{app.var_range_start_var.get()}, {app.var_range_end_var.get()}]")
    update_rule_menus(app)

def add_membership_function(app):
    var_name = app.mf_var_name_var.get()
    if var_name not in app.variables:
        messagebox.showerror("Error", "Variable no encontrada")
        return

    mf_name = app.mf_name_var.get()
    mf_type = app.mf_type_var.get()
    try:
        if mf_type == "trimf":
            mf_params = [float(app.mf_param1_var.get()), float(app.mf_param2_var.get()), float(app.mf_param3_var.get())]
            app.variables[var_name][mf_name] = fuzz.trimf(app.variables[var_name].universe, mf_params)
        elif mf_type == "trapmf":
            mf_params = [float(app.mf_param1_var.get()), float(app.mf_param2_var.get()), float(app.mf_param3_var.get()), float(app.mf_param4_var.get())]
            app.variables[var_name][mf_name] = fuzz.trapmf(app.variables[var_name].universe, mf_params)
        elif mf_type == "gaussmf":
            mf_params = [float(app.mf_param1_var.get()), float(app.mf_param2_var.get())]
            app.variables[var_name][mf_name] = fuzz.gaussmf(app.variables[var_name].universe, mf_params[0], mf_params[1])
        else:
            messagebox.showerror("Error", "Tipo de función de pertenencia no soportado")
            return
    except ValueError:
        messagebox.showerror("Error", "Parámetros inválidos")
        return

    app.mf_listbox.insert(tk.END, f"{var_name}: {mf_name} ({mf_type}, {mf_params})")
    update_rule_menus(app)

def delete_variable(app):
    selected_index = app.var_listbox.curselection()
    if not selected_index:
        return
    selected_index = selected_index[0]
    var_name = app.var_listbox.get(selected_index).split(": ")[1].split(" [")[0]
    del app.variables[var_name]
    app.var_listbox.delete(selected_index)
    update_rule_menus(app)

def delete_membership_function(app):
    selected_index = app.mf_listbox.curselection()
    if not selected_index:
        return
    selected_index = selected_index[0]
    var_name, mf_name = app.mf_listbox.get(selected_index).split(": ")
    mf_name = mf_name.split(" (")[0]
    del app.variables[var_name][mf_name]
    app.mf_listbox.delete(selected_index)
    update_rule_menus(app)

def delete_rule(app):
    selected_index = app.rule_listbox.curselection()
    if not selected_index:
        return
    selected_index = selected_index[0]
    del app.rules[selected_index]
    app.rule_listbox.delete(selected_index)

def clear_all(app):
    app.variables = {}
    app.rules = []
    app.var_listbox.delete(0, tk.END)
    app.mf_listbox.delete(0, tk.END)
    app.rule_listbox.delete(0, tk.END)
    app.rule_antecedents = []
    app.rule_operators = []
    app.rule_consequents = []
    app.current_antecedents_count = 0
    update_rule_menus(app)

def update_rule_menus(app):
    for i, antecedent in enumerate(app.rule_antecedents):
        if i < len(app.rule_frame.grid_slaves(row=2 + i + 1, column=1)):
            menu = app.rule_frame.grid_slaves(row=2 + i + 1, column=1)[0]["menu"]
            menu.delete(0, "end")
            for term in get_variable_terms(app):
                menu.add_command(label=term, command=tk._setit(antecedent, term))

    for i, consequent in enumerate(app.rule_consequents):
        if i < len(app.rule_frame.grid_slaves(row=2 + app.current_antecedents_count + 1, column=1)):
            menu = app.rule_frame.grid_slaves(row=2 + app.current_antecedents_count + 1, column=1)[0]["menu"]
            menu.delete(0, "end")
            for term in get_variable_terms(app):
                menu.add_command(label=term, command=tk._setit(consequent, term))

def get_variable_terms(app):
    terms = []
    for key in app.variables.keys():
        for term in app.variables[key].terms:
            terms.append(f"{key}: {term}")
    return terms
